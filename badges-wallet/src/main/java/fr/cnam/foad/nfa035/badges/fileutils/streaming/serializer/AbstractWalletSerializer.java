package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.IOException;
import java.io.OutputStream;


    public abstract class AbstractWalletSerializer extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia> implements  WalletSerializer<DigitalBadge, WalletFrameMedia> {
        @Override
        public abstract void serialize(DigitalBadge source, WalletFrameMedia media) throws IOException ;
        @Override
        public void rollback(DigitalBadge source, WalletFrameMedia media) throws IOException {
            // TODO
        }

    }




