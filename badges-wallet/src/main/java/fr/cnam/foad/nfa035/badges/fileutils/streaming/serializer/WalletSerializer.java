package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer;


import java.io.IOException;

public interface WalletSerializer<S, M> extends ImageStreamingSerializer<S,M> {

        void rollback(S source, M media) throws IOException;

    }


